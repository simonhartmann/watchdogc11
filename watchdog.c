#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<errno.h>
#include<time.h>
#include<unistd.h>
#include<sys/types.h>
#include<signal.h>

int main(int argc, char* argv[]) {
  char* pid_file_path = NULL;
  char* restart_cmd = NULL;
  const char* help_text = "usage: watchdog <pid_file_path> <restart_command>\n";

  if (argc != 3) {
    fprintf(stdout, "invalid argument count.\n");
    fprintf(stdout, "%s", help_text);
    return -1;
  }

  pid_file_path = argv[1];
  restart_cmd = argv[2];

  if (strcmp(pid_file_path, "") == 0 || strcmp(restart_cmd, "") == 0) {
    fprintf(stdout, "invalid arguments.\n");
    fprintf(stdout, "%s", help_text);
    return -1;
  }

  time_t now = time(NULL);
  fprintf(stdout, "%s: starting watchdog\n", ctime(&now));

  int time_sleep_sec = 30;

  while (true) {
    FILE* pid_file = fopen(pid_file_path, "r");

    if (pid_file != NULL) {
      int pid = 0;
      fscanf(pid_file, "%d", &pid);

      now = time(NULL);

      fprintf(stdout, "%s\n", ctime(&now));
      fprintf(stdout, "checking for process %d\n", pid);

      if (kill(pid, 0) == 0) {
        fprintf(stdout, "process is running %d\n", pid);
      } else if (errno == ESRCH) {
        fprintf(stdout, "process not running %d\n", pid);

        if (system(restart_cmd) == 0) {
          fprintf(stdout, "process restarted.\n");
        } else {
          fprintf(stdout, "failed to restart process %d : %s\n", pid, strerror(errno));
        }
      } else {
        fprintf(stdout, "%s\n", strerror(errno));
      }
    }

    sleep(time_sleep_sec);
  }

 return 1;
}
